<?php

// echo $this->helper->layout->dashboard('kanboard:dashboard/overview', array(
//     'title'              => t('Dashboard for %s', $this->helper->user->getFullname($user)),
//     'user'               => $user,
//     'overview_paginator' => $this->dashboardPagination->getOverview($user['id']),
//     'project_paginator'  => $this->projectPagination->getDashboardPaginator($user['id'], 'show', 30),
// ));
$project_paginator->setMax(50);
?>
<div class="Taeluf-HomePage">
<?=$this->render('kanboard:dashboard/overview', array('user' => $user, 'title'=>$title, 'overview_paginator'=>$overview_paginator, 'project_paginator'=>$project_paginator)) ?>
</div>