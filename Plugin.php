<?php

namespace Kanboard\Plugin\KanboardEssentials;

use Kanboard\Core\Plugin\Base;

class Plugin extends Base
{

    public function initialize(){
        $this->templateOverrides();
        $this->globalCssFiles();   
        $this->globalJsFiles();
        $this->controllerBasedOperations();
        $this->on('template:board:column:header', 'board/Column_Controls');
        
    }

    public function replaceTemplate($existingName, $pluginViewName){
        $this->template->setTemplateOverride($existingName, $this->getPluginName().':'.$pluginViewName);
    }
    public function on($hookName, $templateName){
        $this->template->hook->attach($hookName, $this->getPluginName().':'.$templateName);
    }
    public function addAsset($fileName){
        $pathPrefix = 'plugins/KanboardEssentials/Asset/';
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        $this->hook->on("template:layout:{$ext}", array("template"=> "{$pathPrefix}{$fileName}"));
    }
    
    protected function controllerBasedOperations(){
        $url = $_SERVER['REQUEST_URI'];
        $urlPath = parse_url($url, PHP_URL_PATH);
        $parts = explode('/', $url);
        $place = $parts[1] ?? null;
        $controller = $_GET['controller'] ?? null;
        if ($url==KANBOARD_URL){
            $controller = 'DashboardController';
        } else if ($place=='board'){
            $controller = 'BoardViewController';
        }

        $this->handle($controller, $urlPath);
    }

    protected function handle($controller, $url){
        switch($controller){
            case 'DashboardController':
                $this->addAsset('HomePage.css');
                break;
            case 'BoardViewController':
                $this->addAsset('TaskView.js');
                break;
        }
    }


    public function templateOverrides(){
        $this->replaceTemplate('dashboard/overview', 'home_page');
        $this->replaceTemplate('board/task_private', 'task/card');
        $this->replaceTemplate('task/show', 'task/full');
        $this->replaceTemplate('board/table_container', 'board/main');
        $this->replaceTemplate('column/index', 'column/Edit_Controls');
    }

    public function globalCssFiles(){
        $this->addAsset('main.css');
        $this->addAsset('Modal.css');
        $this->addAsset('TaskContent.css');
    }

    public function globalJsFiles(){
        $this->addAsset('CommentButtonText.js');
    }


    public function getPluginName()
    {
        return 'KanboardEssentials';
    }

    public function getPluginDescription()
    {
        return t('Features I believe are essential for Kanboard');
    }

    public function getPluginAuthor()
    {
        return 'taeluf';
    }

    public function getPluginVersion()
    {
        return '0.0.1';
    }

    public function getPluginHomepage()
    {
        return 'https://gitlab.com/taeluf/php/kanboard-essentials';
    }
}
